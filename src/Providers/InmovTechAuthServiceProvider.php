<?php

namespace Inmovsoftware\LoginApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\LoginApi\Exceptions\ErrorHandler;
use Inmovsoftware\LoginApi\Http\Resources\GlobalCollection;

class InmovTechAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

        $this->mergeAuthFileFrom(__DIR__ . '/../../config/auth.php', 'auth');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->loadMiddleware();
        $this->registerExceptionHandler();
        $this->app->make('Inmovsoftware\LoginApi\Models\Userlogin');
        $this->app->make('Inmovsoftware\LoginApi\Http\Controllers\AuthController');

    }

    protected function mergeAuthFileFrom($path, $key)
    {
        $original = $this->app['config']->get($key, []);
        $this->app['config']->set($key, $this->multi_array_merge(require $path, $original));
    }

    protected function multi_array_merge($toMerge, $original)
    {
        $auth = [];
        foreach ($original as $key => $value) {
            if (isset($toMerge[$key])) {
                $auth[$key] = array_merge($value, $toMerge[$key]);
            } else {
                $auth[$key] = $value;
            }
        }
        return $auth;
    }

    protected function loadMiddleware()
    {
        app()->make('router')->aliasMiddleware('jwt',  \Inmovsoftware\LoginApi\Http\Middleware\jwtMiddleware::class);
        $kernel = $this->app->make('Illuminate\Contracts\Http\Kernel');
    	$kernel->pushMiddleware('Inmovsoftware\LoginApi\Http\Middleware\CorsInmov');
    }

    protected function registerExceptionHandler()
    {
        \App::singleton(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            ErrorHandler::class
        );

        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }

}
